# [`dune-makepkg`](https://gitlab.com/dune-archiso/testing/aur/dune-makepkg) packages for Arch Linux

[![pipeline status](https://gitlab.com/dune-archiso/testing/aur/dune-makepkg/badges/main/pipeline.svg)](https://gitlab.com/dune-archiso/testing/aur/dune-makepkg/-/commits/main)
[![coverage report](https://gitlab.com/dune-archiso/testing/aur/dune-makepkg/badges/main/coverage.svg)](https://gitlab.com/dune-archiso/testing/aur/dune-makepkg/-/commits/main)

This is a third-party auto-updated repository with the following [tarballs](https://gitlab.com/dune-archiso/testing/aur/dune-makepkg/-/raw/main/dune-packages.x86_64).

## [Usage](https://gitlab.com/dune-archiso/testing/aur/dune-makepkg/-/pipelines/latest)

### Add repository

1. Import GPG key from GPG servers.

```console
[user@hostname ~]$ sudo pacman-key --recv-keys 2403871B121BD8BB
[user@hostname ~]$ sudo pacman-key --finger 2403871B121BD8BB
[user@hostname ~]$ sudo pacman-key --lsign-key 2403871B121BD8BB
```

2. Append the following lines to `/etc/pacman.conf`:

```toml
[dune-makepkg]
SigLevel = Required DatabaseOptional
Server = https://dune-archiso.gitlab.io/testing/aur/dune-makepkg/$arch
```

### List packages

To show actual packages list:

```console
[user@hostname ~]$ pacman -Sl dune-makepkg
```

### Install packages

To install package:

```console
[user@hostname ~]$ sudo pacman -Syu
[user@hostname ~]$ sudo pacman -S dune-fem
```

Powered by Arch Linux for Education (`arch4edu`).

[Running every friday at 4:00 AM UTC-5](https://gitlab.com/dune-archiso/testing/dune-makepkg/-/pipeline_schedules).